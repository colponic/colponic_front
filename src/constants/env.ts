export const API_URL = `${process.env.REACT_APP_API_URL}`;
export const API_CLIENT_ID = `${process.env.REACT_APP_CLIENT_ID}`;
