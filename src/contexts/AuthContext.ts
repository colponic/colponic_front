import {createContext, useContext} from "react";
import {Token} from "../models/Token";

export interface AuthInstance {
  isLoggedIn: boolean;
  logIn: (username: string, password: string) => Promise<void>;
  logOut: () => Promise<void>;
  refreshToken: () => Promise<string>;
  token: Token | null;
  loaded: boolean;
  email:string | null;
}

export const fakeAuthInstance: AuthInstance = {
  isLoggedIn: false,
  logIn: async (_username: string, _password: string) => {},
  logOut: async () => {},
  refreshToken: async () => "",
  token: null,
  loaded: false,
  email: null
};

export const AuthContext = createContext<AuthInstance>(fakeAuthInstance);
export const useAuth = () => useContext(AuthContext);
