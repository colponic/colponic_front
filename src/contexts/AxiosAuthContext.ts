import { useContext, createContext } from "react";
import axios from "axios";

const axiosInstance = axios.create();
export const AxiosAuthContext = createContext(axiosInstance);

export const useAxiosAuthInstance = () => useContext(AxiosAuthContext);