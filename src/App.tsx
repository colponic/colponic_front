import React, {useEffect, useState} from 'react';
import {BrowserRouter} from "react-router-dom";
import axios from "axios";
import {AuthTokenService} from "./services/AuthTokenService";
import {AxiosAuthContext} from "./contexts/AxiosAuthContext";
import {AuthContext, AuthInstance, fakeAuthInstance} from "./contexts/AuthContext";
import MainRoutes from "./pages/main_routes/MainRoutes";
import {API_URL} from "./constants/env";
import {Token} from "./models/Token";
import {createTheme, Theme, ThemeProvider} from "@material-ui/core";


const theme: Theme = createTheme({
  palette: {
    primary: {
      main: "#00961e",
    },
    secondary: {
      main: "#f0f0f0",
    },
  },
  typography: {
    fontFamily: ["Montserrat"].join(","),
  },
});

const axiosInstance = axios.create({
  baseURL: API_URL,
});

const axiosAuthInstance = axios.create({
  baseURL: API_URL,
});

function App() {
  const [auth, setAuth] = useState<AuthInstance>(fakeAuthInstance);
  const [authTokenService] = useState(new AuthTokenService(axiosInstance));


  useEffect(() => {
    const subscription = authTokenService.tokenObservable.subscribe((token: Token | null) => {
      setAuth({
        isLoggedIn: token != null,
        logIn: async (username: string, password: string) => {
          await authTokenService.getCredentials(username, password);
        },
        logOut: async () => {
          authTokenService.setToken(null);
        },
        refreshToken: async () => {
          return await authTokenService.refreshCredentials();
        },
        token: token,
        loaded: true,
        email: token?.email || null
      });
    });
    return () => {
      subscription.unsubscribe();
    }
  }, [authTokenService]);



  return (
    <ThemeProvider theme={theme}>
    <AuthContext.Provider value={auth}>
        <AxiosAuthContext.Provider value={axiosAuthInstance}>
            <BrowserRouter>
              <MainRoutes/>
            </BrowserRouter>
        </AxiosAuthContext.Provider>
    </AuthContext.Provider>
    </ThemeProvider>
  );
}

export default App;

