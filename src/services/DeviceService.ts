import { AxiosInstance } from "axios";
import Device from "../models/Device";
import SensorValue from "../models/SensorValue";
import Actuator from "../models/Actuator";
import Alarm from "../models/Alarm";

export class DeviceService {
  constructor(private _axios: AxiosInstance) {}

  public async getDevices(): Promise<Device[]> {
    const url: string = `/devices/list`;
    const response: any = await this._axios.get(url);
    return response.data.map(
      (s: any): Device => ({
        id: s.id,
        name: s.name,
        photo: s.photo,
        startDate: s.start_date,
        endDate: s.end_date,
        state: s.state,
        location: s.location,
      })
    );
  }

  public async getDevice(deviceId: string): Promise<Device> {
    const url: string = `/devices/${deviceId}`;
    const response: any = await this._axios.get(url);
    const res: any = response.data;
    return {
      id: res.id,
      name: res.name,
      photo: res.photo,
      location: res.location,
      startDate: res.start_date,
      endDate: res.end_date,
      state: res.state,
    };
  }

  public async getSensorValue(device_id: string): Promise<SensorValue[]> {
    const url: string = `/devices/read_sensor/${device_id}`;
    const response: any = await this._axios.get(url);
    return response.data.map(
      (s: any): SensorValue => ({
        id: s.id,
        readingDate: s.reading_data,
        sensor: s.sensor,
        unit: s.unit,
        value: s.value,
      })
    );
  }

  public async getActuators(device_id: string): Promise<Actuator[]> {
    const url: string = `/devices/read_actuators/${device_id}`;
    const response: any = await this._axios.get(url);
    return response.data.map(
      (s: any): Actuator => ({
        id: s.id,
        name: s.name,
        state: s.state,
        timeOn: s.time_on,
        timeOff: s.time_off,
        device: s.device,
      })
    );
  }

  public async getAlarms(device_id: string): Promise<Alarm[]> {
    const url: string = `/devices/alarms/${device_id}`;
    const response: any = await this._axios.get(url);
    return response.data.map(
      (s: any): Alarm => ({
        id: s.id,
        description: s.description,
        viewed: s.state,
        createdAt: s.created_at,
        sensor: s.sensor,
        sensorName: s.sensor_name,
      })
    );
  }
}
