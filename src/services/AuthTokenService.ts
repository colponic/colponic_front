import {BehaviorSubject, Observable} from "rxjs";
import {AxiosInstance, AxiosResponse} from "axios";
import {API_CLIENT_ID} from "../constants/env";
import createFormData from "../utils/axios";
import {Token} from "../models/Token";

export class AuthTokenService {
  private static readonly localStorageKey = "token";

  private _tokenObservable = new BehaviorSubject<Token | null>(null);
  public get tokenObservable(): Observable<Token | null> { return this._tokenObservable; }

  private _refreshPromise: Promise<string> | null = null;

  constructor(private _axios: AxiosInstance) {
    this.loadToken();
  }

  private loadToken() {
    const token = AuthTokenService.getTokenFromLocalStorage();
    this._tokenObservable.next(token);
  }

  public getToken(): Token | null {
    return this._tokenObservable.value;
  }

  public setToken(token: Token | null) {
    if (token !== null) {
      const objectToken = {
        accessToken: token.accessToken,
        refreshToken: token.refreshToken,
        email:token.email
      };
      const stringToken = JSON.stringify(objectToken);
      localStorage.setItem(AuthTokenService.localStorageKey, stringToken);
    } else {
      localStorage.removeItem(AuthTokenService.localStorageKey);
    }
    this.loadToken();
  }

  public async getCredentials(username: string, password: string): Promise<void> {
    const formData = createFormData({
      client_id: API_CLIENT_ID,
      grant_type: "password",
      username: username,
      password: password,
    });

    const response: AxiosResponse = await this._axios.post("auth/login/", formData, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    });
    // @ts-ignore
    const token = new Token(response.data.access_token, response.data.refresh_token, username);
    this.setToken(token);
  }

  /*
    This function refreshes the credentials from the refresh token stored locally.
    It ensures that if two processes call it at the *same* time, only the first one is the one that actually creates the
    promise that refreshes them, and all the subsequent calls, will await on that already created promise, so that
    the credentials are not refreshed multiple times. In the end it will return the new access token obtained from the
    api.
  */
  public async refreshCredentials(): Promise<string> {
    if (this._refreshPromise != null) {
      return await this._refreshPromise;
    } else {
      const refreshPromise: Promise<string> = new Promise(async (resolve, reject) => {

        const refreshToken: string | undefined = this.getToken()?.refreshToken;
        if (refreshToken === undefined) {
          throw Error('No refreshToken found locally');
        }

        try {
          const formData = createFormData({
            client_id: API_CLIENT_ID,
            grant_type: "refresh_token",
            refresh_token: refreshToken,
          });
          const response: AxiosResponse = await this._axios.post("auth/login/", formData, {
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
            },
          });
          // @ts-ignore
          const token = new Token(response.data.access_token, response.data.refresh_token, response.data.username);
          this.setToken(token);
          resolve(token.accessToken);
        } catch (e) {
          reject(e);
        }
      });

      this._refreshPromise = refreshPromise
      const newAccessToken: string = await this._refreshPromise;
      this._refreshPromise = null;

      return newAccessToken;
    }
  }

  public static getTokenFromLocalStorage(): Token | null {
    const stringToken: string | null = localStorage.getItem(AuthTokenService.localStorageKey);

    if (stringToken !== null) {
      try {
        const localToken = JSON.parse(stringToken);
        let accessToken = null;
        let refreshToken = null;
        let email = null;
        if (
          localToken.hasOwnProperty("accessToken") &&
          typeof localToken.accessToken === "string"
        ) {
          accessToken = localToken.accessToken;
        }
        if (
          localToken.hasOwnProperty("refreshToken") &&
          typeof localToken.refreshToken === "string"
        ) {
          refreshToken = localToken.refreshToken;
        }
        if (
          localToken.hasOwnProperty("email") &&
          typeof localToken.email === "string"
        ) {
          email = localToken.email;
        }
        if (accessToken && refreshToken && email) {
          return new Token(accessToken, refreshToken, email);
        }
        return null;
      } catch {
        return null;
      }
    }
    return null;
  }
}
