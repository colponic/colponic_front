export class Token {
  private readonly _accessToken: string;
  public get accessToken(): string { return this._accessToken; }

  private readonly _refreshToken: string;
  public get refreshToken(): string { return this._refreshToken; }

  private readonly _email: string;
  public get email(): string { return this._email; }

  constructor(accessToken: string, refreshToken: string, email:string) {
    this._accessToken = accessToken;
    this._refreshToken = refreshToken;
    this._email = email;
  }
}