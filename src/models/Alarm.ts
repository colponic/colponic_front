export default interface Alarm {
  id: number;
  description: string;
  viewed: boolean;
  createdAt: Date;
  sensor: string;
  sensorName: string;
}
