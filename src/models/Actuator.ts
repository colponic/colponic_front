export default interface Actuator {
  id: number;
  name: string;
  state: boolean;
  timeOn: number;
  timeOff: number;
  device: number;
}
