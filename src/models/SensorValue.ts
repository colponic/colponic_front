export default interface SensorValue {
  id: number;
  readingDate: Date;
  sensor: number;
  unit: string;
  value: string;
}
