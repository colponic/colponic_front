export default interface Device {
  id: number;
  name: string;
  photo: string | null;
  startDate: string;
  endDate: string;
  state: boolean;
  location: string;
}