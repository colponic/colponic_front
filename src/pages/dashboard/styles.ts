import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  row: {
    display: "flex",
    flexDirection: "row",
  },
  column: {
    display: "flex",
    flexDirection: "column",
  },
  center: {
    alignSelf: "center",
    paddingTop: 25,
  },
  container: {
    width: 400,
    borderRadius: 8,
    height: 350,
    margin: "0 50px 50px 50px",
    justifyContent: "center",
    border: "1px dashed #111111",
  },
  containerTitle: {
    backgroundColor: "#C0C0BF",
    borderBottom: "1px dashed #111111",
    color: "black",
    textAlign: "center",
    padding: "10px 0",
    fontWeight: 600,
    fontSize: "1.2rem",
    borderRadius: "8px 8px 0 0",
    marginBottom: 10,
  },
  containerText: {
    marginBottom: "0",
    padding: "0 10px",
  },
  listNames: {
    margin: 10,
    flexGrow: 1,
    fontWeight: 600,
  },
  listValues: {
    margin: 10,
    flexGrow: 1,
    textAlign: "right",
    fontWeight: 600,
  },
  line: {
    border: "1px dashed grey",
  },
  onStyle: {
    color: "#000000",
    backgroundColor: "#FFFFFF",
    textAlign: "center",
    border: "1px solid #111111",
  },
  offStyle: {
    color: "#000000",
    backgroundColor: "#79797A",
    textAlign: "center",
    border: "1px solid #111111",
  },
  scroll: {
    overflow: "auto",
  },
});

export default useStyles;
