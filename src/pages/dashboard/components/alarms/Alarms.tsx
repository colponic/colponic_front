import useStyles from "../../styles";
import Alarm from "../../../../models/Alarm";
import clsx from "clsx";

interface Props {
  alarms: Alarm[];
}

const changeWord = (word: string) => {
  let newWord: string = "Sensor de temperatura 2";
  if (word === "temp-0") {
    newWord = "Sensor de temperatura";
  } else if (word === "pressure") {
    newWord = "Sensor de presión";
  } else if (word === "Altitud") {
    newWord = "Sensor de altura";
  } else if (word === "lux") {
    newWord = "Sensor de luminosidad";
  }
  return newWord;
};

const Alarms = ({ alarms }: Props) => {
  const classes = useStyles();
  return (
    <div className={clsx(classes.container, classes.scroll)}>
      <div className={classes.containerTitle}>Notificaciones</div>
      <div style={{ padding: 10 }}>
        {alarms.map((alarm: Alarm) => {
          return (
            <p
              style={{
                backgroundColor: "#FE115D",
                padding: 5,
                borderRadius: 5,
              }}
            >
              {changeWord(alarm.sensorName)} - {alarm.description} -{" "}
              {new Intl.DateTimeFormat("es-CO", {
                year: "numeric",
                month: "short",
                day: "numeric",
                hour: "numeric",
                minute: "numeric",
                second: "numeric",
              }).format(new Date(alarm.createdAt))}
            </p>
          );
        })}
      </div>
    </div>
  );
};

export default Alarms;
