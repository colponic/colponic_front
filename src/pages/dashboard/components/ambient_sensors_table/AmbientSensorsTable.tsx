import useStyles from "../../styles";
import clsx from "clsx";
import SensorValue from "../../../../models/SensorValue";

interface Props {
  sensorValues: SensorValue[];
}

const AmbientSensorsTable = ({ sensorValues }: Props) => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div className={classes.containerTitle}>Sensores de Ambiente</div>
      <div className={clsx(classes.row, classes.containerText)}>
        <div className={clsx([classes.column, classes.listNames])}>
          <p>Temperatura</p>
          <p>Presión</p>
          <p>Altitud</p>
          <p>Luminosidad</p>
          <p>Temperatura 2</p>
        </div>
        <div className={classes.line} />
        <div className={clsx(classes.column, classes.listValues)}>
          {sensorValues.map((sensorValue: SensorValue) => {
            return (
              <p key={sensorValue.id}>
                {sensorValue.value} {sensorValue.unit}
              </p>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default AmbientSensorsTable;
