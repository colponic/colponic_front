import useStyles from "../../styles";
import clsx from "clsx";

const SolutionSensorsTable = () => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div className={classes.containerTitle}>Sensores en la solución</div>
      <div className={clsx(classes.row, classes.containerText)}>
        <div className={clsx([classes.column, classes.listNames])}>
          <p>Temperatura</p>
          <p>Conductividad Eléctrica</p>
          <p>pH</p>
        </div>
        <div className={classes.line} />
        <div className={clsx(classes.column, classes.listValues)}>
          <p>{Math.floor(Math.random() * (26 - 18)) + 18} cº</p>
          <p>{Math.floor(Math.random() * (1150 - 736)) + 736} ppm</p>
          <p>{Math.round((Math.random() * (6.4 - 5.6) + 5.6) * 10) / 10}</p>
        </div>
      </div>
    </div>
  );
};

export default SolutionSensorsTable;
