import useStyles from "../../styles";
import clsx from "clsx";
import Actuator from "../../../../models/Actuator";

interface Props {
  actuators: Actuator[];
}

const StateActuators = ({ actuators }: Props) => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div className={classes.containerTitle}>Actuadores</div>
      <div className={clsx(classes.row, classes.containerText)}>
        <div className={clsx([classes.column, classes.listNames])}>
          <p>Motobomba</p>
          <p>Iluminarias LED</p>
        </div>
        <div className={classes.line} />
        <div className={clsx(classes.column, classes.listValues)}>
          {actuators.map((actuator: Actuator) => {
            return (
              <p
                key={actuator.id}
                className={actuator.state ? classes.onStyle : classes.offStyle}
              >
                {actuator.state ? "Encendido" : "Apagado"}
              </p>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default StateActuators;
