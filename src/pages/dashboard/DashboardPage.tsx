import useStyles from "./styles";
import Sidebar from "../../components/sidebar/Sidebar";
import clsx from "clsx";
import { useCallback, useEffect, useMemo, useState } from "react";
import ProcessStatusContent from "../../components/ProcessStatusContent";
import {
  Exception,
  Idle,
  Processing,
  ProcessStatus,
  Success,
} from "../../core/Process";
import SensorValue from "../../models/SensorValue";
import { useAxiosAuthInstance } from "../../contexts/AxiosAuthContext";
import { DeviceService } from "../../services/DeviceService";
import { CircularProgress } from "@material-ui/core";
import { useLocation } from "react-router-dom";
import AmbientSensorsTable from "./components/ambient_sensors_table/AmbientSensorsTable";
import SolutionSensorsTable from "./components/solution_sensors_table/SolutionSensorsTable";
import Actuator from "../../models/Actuator";
import Alarm from "../../models/Alarm";
import StateActuators from "./components/state_actuators/StateActuators";
import Alarms from "./components/alarms/Alarms";

const DashboardPage = () => {
  const classes = useStyles();
  const axios = useAxiosAuthInstance();
  const location = useLocation();
  // @ts-ignore
  const deviceId = location.state["deviceId"];
  const deviceService = useMemo(() => {
    return new DeviceService(axios);
  }, [axios]);
  const [sensorValueProcess, setSensorValueProcess] = useState<
    ProcessStatus<SensorValue[]>
  >(new Idle());
  const [actuatorStateProcess, setActuatorStateProcess] = useState<
    ProcessStatus<Actuator[]>
  >(new Idle());
  const [alarmsProcess, setAlarmsProcess] = useState<ProcessStatus<Alarm[]>>(
    new Idle()
  );
  const loadSensorValues = useCallback(async () => {
    try {
      setSensorValueProcess(new Processing());
      const sensorValues: SensorValue[] = await deviceService.getSensorValue(
        deviceId
      );
      setSensorValueProcess(new Success(sensorValues));
    } catch (e: any) {
      setSensorValueProcess(new Exception(e));
    }
  }, [deviceService, deviceId]);

  const loadActuatorState = useCallback(async () => {
    try {
      setActuatorStateProcess(new Processing());
      const actuators: Actuator[] = await deviceService.getActuators(deviceId);
      setActuatorStateProcess(new Success(actuators));
    } catch (e: any) {
      setActuatorStateProcess(new Exception(e));
    }
  }, [deviceService, deviceId]);

  const loadAlarms = useCallback(async () => {
    try {
      setAlarmsProcess(new Processing());
      const alarms: Alarm[] = await deviceService.getAlarms(deviceId);
      setAlarmsProcess(new Success(alarms));
    } catch (e: any) {
      setAlarmsProcess(new Exception(e));
    }
  }, [deviceService, deviceId]);

  useEffect(() => {
    loadSensorValues();
    loadActuatorState();
    loadAlarms();
  }, [loadSensorValues, loadActuatorState, loadAlarms]);

  return (
    <div className={clsx(classes.row, classes.center)}>
      <Sidebar />
      <div className={classes.column}>
        <div className={classes.row}>
          <ProcessStatusContent
            processStatus={sensorValueProcess}
            idleContent={null}
            processingContent={
              <>
                <CircularProgress />
              </>
            }
            exceptionContentFactory={null}
            successContentFactory={(value) => (
              <AmbientSensorsTable sensorValues={value} />
            )}
            sameExceptionAsIdle={true}
          />
          <SolutionSensorsTable />
        </div>
        <div className={classes.row}>
          <ProcessStatusContent
            processStatus={actuatorStateProcess}
            idleContent={null}
            processingContent={
              <>
                <CircularProgress />
              </>
            }
            exceptionContentFactory={null}
            successContentFactory={(value) => (
              <StateActuators actuators={value} />
            )}
            sameExceptionAsIdle={true}
          />
          <ProcessStatusContent
            processStatus={alarmsProcess}
            idleContent={null}
            processingContent={
              <>
                <CircularProgress />
              </>
            }
            exceptionContentFactory={null}
            successContentFactory={(value) => <Alarms alarms={value} />}
            sameExceptionAsIdle={true}
          />
        </div>
      </div>
    </div>
  );
};

export default DashboardPage;
