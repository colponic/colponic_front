import { Switch, Route, useHistory, Redirect } from "react-router-dom";

import useStyles from "./styles";
import LoginPage from "../../pages/login/LoginPage";

import PrivateRoute from "../../components/PrivateRoute";
import React, { useEffect, useMemo, useState } from "react";
import { AuthInstance, useAuth } from "../../contexts/AuthContext";
import { AxiosError, AxiosInstance, AxiosResponse } from "axios";
import { useAxiosAuthInstance } from "../../contexts/AxiosAuthContext";
import DevicesPage from "../device/DevicesPage";
import Navbar from "../../components/navbar/Navbar";
import DashboardPage from "../dashboard/DashboardPage";
import AlarmPage from "../alarm/AlarmPage";

const MainRoutes = () => {
  const classes = useStyles();

  const axiosAuth: AxiosInstance = useAxiosAuthInstance();
  const history = useHistory();
  const auth: AuthInstance = useAuth();
  const [interceptorsAdded, setInterceptorsAdded] = useState(false);

  useEffect(() => {
    if (auth.loaded) {
      setInterceptorsAdded(false);
      const requestInterceptor: number = axiosAuth.interceptors.request.use(
        async (config) => {
          // @ts-ignore
          if (!config.headers.hasOwnProperty("Authorization")) {
            // @ts-ignore
            config.headers[
              "Authorization"
            ] = `Bearer ${auth.token?.accessToken}`;
          }
          return config;
        },
        (error: AxiosError) => Promise.reject(error)
      );
      const responseInterceptor: number = axiosAuth.interceptors.response.use(
        (response: AxiosResponse) => response,
        async (error: AxiosError) => {
          const originalConfig = error.config;
          if (error.response?.status === 401) {
            // @ts-ignore
            if (!originalConfig.headers.hasOwnProperty("X-Retried-From")) {
              // This is the first time this request fails with a 401, so we try to refresh the credentials and then
              // we'll retry it again with the new ones
              try {
                const newAccessToken: string = await auth.refreshToken();
                // @ts-ignore
                originalConfig.headers["X-Retried-From"] = `${
                  originalConfig.headers
                    ? originalConfig.headers["Authorization"]
                    : ""
                }`;
                // @ts-ignore
                originalConfig.headers[
                  "Authorization"
                ] = `Bearer ${newAccessToken}`;
                return axiosAuth(originalConfig);
              } catch (e) {
                // We logout because the server returned an error, so the credentials could not be refreshed
                await auth.logOut();
              }
            } else {
              // We logout because although credentials were refreshed, for whatever reason the retry failed again
              // with a 401
              await auth.logOut();
            }
          }
          return Promise.reject(error);
        }
      );
      setInterceptorsAdded(true);
      return () => {
        axiosAuth.interceptors.request.eject(requestInterceptor);
        axiosAuth.interceptors.response.eject(responseInterceptor);
      };
    }
  }, [history, axiosAuth, auth]);

  const appReady = useMemo(() => {
    const ready: boolean = auth.loaded && interceptorsAdded;
    return ready;
  }, [auth.loaded, interceptorsAdded]);

  return (
    <>
      {appReady ? (
        <div className={classes.root}>
          <Navbar />
          <Switch>
            <Route path="/login">
              <LoginPage />
            </Route>
            <PrivateRoute path="/devices">
              <DevicesPage />
            </PrivateRoute>
            <PrivateRoute path="/dashboard">
              <DashboardPage />
            </PrivateRoute>
            <PrivateRoute path="/dashboard">
              <DashboardPage />
            </PrivateRoute>
            <PrivateRoute path="/alarms">
              <AlarmPage />
            </PrivateRoute>
            <Route path="/">
              <Redirect to={"/devices"} />
            </Route>
          </Switch>
        </div>
      ) : null}
    </>
  );
};

export default MainRoutes;
