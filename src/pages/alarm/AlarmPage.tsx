import useStyles from "./styles";
import clsx from "clsx";
import Sidebar from "../../components/sidebar/Sidebar";

// @ts-ignore
const times = [...Array(61).keys()];

const AlarmPage = () => {
  const classes = useStyles();
  return (
    <div className={clsx(classes.row, classes.center)}>
      <Sidebar />
      <div className={classes.column}>
        <div className={classes.titlePage}>
          <p className={classes.alarmText}>Alarmas</p>
          <button className={classes.saveButton}>Guardar</button>
        </div>
        <div className={classes.row}>
          <div className={classes.container}>
            <div className={classes.containerTitle}>Sensores de Ambiente</div>
            <div className={clsx(classes.row, classes.containerText)}>
              <div className={clsx([classes.column, classes.listNames])}>
                <p>Luminosidad</p>
                <p>Temperatura</p>
                <p>Altitud</p>
                {/*<p>Presión</p>*/}
                <p>Humedad Relativa</p>
              </div>
              <div className={classes.line} />
              <div className={clsx(classes.column, classes.listValues)}>
                <div className={clsx(classes.row, classes.containerInput)}>
                  <input className={classes.inputText} value={"1500"} />
                  <p style={{ display: "ruby" }}>&lt; x &lt;</p>
                  <input className={classes.inputText} value={"-"} />
                </div>
                <div className={clsx(classes.row, classes.containerInput)}>
                  <input className={classes.inputText} value={"13"} />
                  <p style={{ display: "ruby" }}>&lt; x &lt;</p>
                  <input className={classes.inputText} value={"22"} />
                </div>
                <div className={clsx(classes.row, classes.containerInput)}>
                  <input className={classes.inputText} value={"1800"} />
                  <p style={{ display: "ruby" }}>&lt; x &lt;</p>
                  <input className={classes.inputText} value={"2800"} />
                </div>
                {/*<div className={clsx(classes.row, classes.containerInput)}>*/}
                {/*  <input className={classes.inputText} />*/}
                {/*  <p>:</p>*/}
                {/*  <input className={classes.inputText} />*/}
                {/*</div>*/}
                <div className={clsx(classes.row, classes.containerInput)}>
                  <input className={classes.inputText} value={"67"} />
                  <p style={{ display: "ruby" }}>&lt; x &lt;</p>
                  <input className={classes.inputText} value={"82"} />
                </div>
              </div>
              <div className={clsx(classes.column, classes.listValues)}>
                <p>lux</p>
                <p>cº</p>
                <p>msnm</p>
                {/*<p>pa</p>*/}
                <p>%</p>
              </div>
            </div>
          </div>
          <div className={classes.container}>
            <div className={classes.containerTitle}>Sensores de Ambiente</div>
            <div className={clsx(classes.row, classes.containerText)}>
              <div className={clsx([classes.column, classes.listNames])}>
                <p>Temperatura</p>
                <p>Conductividad Eléctrica</p>
                <p>pH</p>
              </div>
              <div className={classes.line} />
              <div className={clsx(classes.column, classes.listValues)}>
                <div className={clsx(classes.row, classes.containerInput)}>
                  <input className={classes.inputText} value={"20"} />
                  <p style={{ display: "ruby" }}>&lt; x &lt;</p>
                  <input className={classes.inputText} value={"30"} />
                </div>
                <div className={clsx(classes.row, classes.containerInput)}>
                  <input className={classes.inputText} value={"736"} />
                  <p style={{ display: "ruby" }}>&lt; x &lt;</p>
                  <input className={classes.inputText} value={"1150"} />
                </div>
                <div className={clsx(classes.row, classes.containerInput)}>
                  <input className={classes.inputText} value={"5.6"} />
                  <p style={{ display: "ruby" }}>&lt; x &lt;</p>
                  <input className={classes.inputText} value={"6.4"} />
                </div>
              </div>
              <div className={clsx(classes.column, classes.listValues)}>
                <p>cº</p>
                <p>ppm</p>
                <p></p>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.titlePage}>
          <p className={classes.alarmText}>Actuadores</p>
          <button className={classes.saveButton}>Guardar</button>
        </div>
        <div className={clsx(classes.column, classes.margin)}>
          <div className={clsx(classes.row, classes.headerTableActuator)}>
            <p className={classes.headerTableActuatorText}>Actuador</p>
            <p className={classes.headerTableActuatorText}>
              Tiempo de encendido
            </p>
            <p className={classes.headerTableActuatorText}>Tiempo de apagado</p>
          </div>
          <div className={classes.row}>
            <p className={classes.actuatorName}>Motor 1</p>
            <div className={clsx(classes.actuatorInputContainer, classes.row)}>
              <select>
                <option>10</option>
                {times.map((n) => {
                  return <option key={n.toString()}>{n}</option>;
                })}
              </select>
              <p>minutos</p>
            </div>
            <div className={clsx(classes.actuatorInputContainer, classes.row)}>
              <select>
                <option>40</option>
                {times.map((n) => {
                  return <option key={n.toString()}>{n}</option>;
                })}
              </select>
              <p>minutos</p>
            </div>
          </div>
          <div className={classes.row}>
            <p className={classes.actuatorName}>Motor 2</p>
            <div className={clsx(classes.actuatorInputContainer, classes.row)}>
              <select>
                <option>10</option>
                {times.map((n) => {
                  return <option key={n.toString()}>{n}</option>;
                })}
              </select>
              <p>minutos</p>
            </div>
            <div className={clsx(classes.actuatorInputContainer, classes.row)}>
              <select>
                <option>50</option>
                {times.map((n) => {
                  return <option key={n.toString()}>{n}</option>;
                })}
              </select>
              <p>minutos</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AlarmPage;
