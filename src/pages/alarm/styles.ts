import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  row: {
    display: "flex",
    flexDirection: "row",
  },
  column: {
    display: "flex",
    flexDirection: "column",
  },
  center: {
    alignSelf: "center",
    paddingTop: 25,
  },
  titlePage: {
    backgroundColor: "#C0C0BF",
    position: "relative",
    borderRadius: 12,
    margin: "0 50px 25px 50px",
  },
  inputText: {
    width: 50,
    height: 20,
    border: "none",
    backgroundColor: "#f5f5f5",
    padding: "0 5px 0 5px",
    textAlign: "center",
    "&:focus": {
      outline: "2px solid #2C7634",
    },
  },
  alarmText: {
    textAlign: "center",
    color: "#000",
    fontWeight: 600,
    fontSize: "1.5rem",
    margin: 15,
  },
  saveButton: {
    position: "absolute",
    right: 10,
    top: 15,
    cursor: "pointer",
    backgroundColor: "#979695",
    padding: 10,
    borderRadius: 8,
    color: "#000",
    fontWeight: 600,
  },
  container: {
    width: 400,
    borderRadius: 8,
    height: 350,
    margin: "0 50px 50px 50px",
    justifyContent: "center",
    border: "1px dashed #111111",
  },
  containerTitle: {
    backgroundColor: "#C0C0BF",
    borderBottom: "1px dashed #111111",
    color: "#000",
    textAlign: "center",
    padding: "10px 0",
    fontWeight: 600,
    fontSize: "1.2rem",
    borderRadius: "8px 8px 0 0",
    marginBottom: 10,
  },
  containerText: {
    marginBottom: "0",
    padding: "0 10px",
  },
  listNames: {
    margin: 10,
    flexGrow: 1,
    fontWeight: 600,
  },
  listValues: {
    margin: 10,
    flexGrow: 1,
    textAlign: "right",
    fontWeight: 600,
  },
  line: {
    border: "1px solid grey",
  },
  containerInput: {
    alignItems: "center",
  },
  headerTableActuator: {
    backgroundColor: "#C0C0BF",
    borderRadius: "8px 8px 0 0",
  },
  headerTableActuatorText: {
    color: "#000",
    flexGrow: 1,
    textAlign: "center",
  },
  actuatorName: { flexGrow: 1, textAlign: "center" },
  actuatorInput: { flexGrow: 1, height: 20 },
  actuatorInputContainer: { flexGrow: 1, justifyContent: "center" },
  margin: { margin: "0 50px 25px 50px" },
});

export default useStyles;
