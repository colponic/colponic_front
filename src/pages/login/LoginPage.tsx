import {
  Button,
  CircularProgress,
  TextField,
  Grid,
  Typography,
} from "@material-ui/core";
import { useCallback, useMemo, useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Exception,
  Idle,
  Processing,
  ProcessStatus,
  Success,
} from "../../core/Process";
import useStyles from "./styles";
import ProcessStatusContent from "../../components/ProcessStatusContent";
import { useAuth } from "../../contexts/AuthContext";
import ColponicLogo from "../../assets/logos/colponic.png";

interface Form {
  error: null | string;
}

interface FormControl<T> {
  value: T;
  error: null | string;
}

interface LoginForm extends Form {
  email: FormControl<string>;
  password: FormControl<string>;
  rememberMe: FormControl<boolean>;
}

const LoginPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const auth = useAuth();
  const [loginProcess, setLoginProcess] = useState<ProcessStatus<boolean>>(
    new Idle()
  );
  const [formData, setFormData] = useState<LoginForm>({
    email: {
      value: "",
      error: null,
    },
    password: {
      value: "",
      error: null,
    },
    rememberMe: {
      value: true,
      error: null,
    },
    error: null,
  });
  const loginSubmitEnabled = useMemo(() => {
    const emailIsValid: boolean = formData.email.value.length !== 0;
    const passwordIsValid: boolean = formData.password.value.length !== 0;
    return emailIsValid && passwordIsValid;
  }, [formData.email.value, formData.password.value]);
  const onSubmitLoginForm = useCallback(async () => {
    try {
      setLoginProcess(new Processing());
      await auth.logIn(formData.email.value, formData.password.value);
      auth.email = formData.email.value;
      setLoginProcess(new Success(true));
      let redirectPath: string;
      const searchParams = new URLSearchParams(history.location.search);
      const next = searchParams.get("next");
      redirectPath = next || "/";
      history.push(redirectPath);
    } catch (e: any) {
      setLoginProcess(new Exception(e));
      if (e?.response?.status === 400) {
        setFormData((prevState) => {
          const newState = Object.assign({}, prevState);
          newState.error = "Credenciales incorrectas";
          return newState;
        });
      }
    }
  }, [auth, formData.email.value, formData.password.value, history]);
  return (
    <ProcessStatusContent
      processStatus={loginProcess}
      idleContent={
        <>
          <Grid container className={classes.root}>
            <Grid
              item
              xs={false}
              sm={4}
              md={7}
              style={{
                backgroundImage: "url(https://source.unsplash.com/random)",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                backgroundPosition: "center",
              }}
            />
            <Grid item xs={false} className={classes.containerForm}>
              <img src={ColponicLogo} alt={"Colponic"} height={80} />
              <form
                className={classes.form}
                onSubmit={async (event) => {
                  event.preventDefault();
                  await onSubmitLoginForm();
                }}
              >
                <TextField
                  color="primary"
                  className={classes.inputText}
                  value={formData.email.value}
                  variant={"outlined"}
                  onChange={(event) => {
                    setFormData((prevState) => {
                      const newState = Object.assign({}, prevState);
                      newState.email.value = event.target.value;
                      return newState;
                    });
                  }}
                  label={"Email"}
                  placeholder={"Email"}
                  type="text"
                />
                <TextField
                  className={classes.inputText}
                  value={formData.password.value}
                  variant={"outlined"}
                  onChange={(event) => {
                    setFormData((prevState) => {
                      const newState = Object.assign({}, prevState);
                      newState.password.value = event.target.value;
                      return newState;
                    });
                  }}
                  label={"Password"}
                  placeholder={"password"}
                  type="password"
                />
                {formData.error != null ? (
                  <Typography variant={"caption"} color={"error"}>
                    {formData.error}
                  </Typography>
                ) : null}
                <Button
                  disabled={!loginSubmitEnabled}
                  variant="contained"
                  color="secondary"
                  type="submit"
                >
                  Login
                </Button>
              </form>
            </Grid>
          </Grid>
        </>
      }
      processingContent={
        <>
          <CircularProgress />
        </>
      }
      exceptionContentFactory={null}
      successContentFactory={null}
      sameExceptionAsIdle={true}
    />
  );
};
export default LoginPage;
