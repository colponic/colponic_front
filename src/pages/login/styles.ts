import {makeStyles} from "@material-ui/core";
//import { lightGreen } from "@material-ui/core/colors";

const useStyles = makeStyles({
  root: {
    height:"calc(100vh - 64px)"
  },
  form:{
    display:"flex",
    flexDirection:"column",
    width:300,
    marginTop:80
  },
  containerForm:{
    display:"flex",
    flexGrow:1,
    flexDirection:"column",
    justifyContent:"center",
    alignItems: "center"
  },
  inputText:{
    margin:"10px 0"
  },
  textWelcome:{
    textAlign:"start",
    alignSelf:"flex-start",
    margin:"20px 0 25px 80px"
  },
})

export default useStyles