import {makeStyles, createStyles, Theme} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height:"calc(100vh - 64px)"
    },
    containerDevices:{
      flexGrow:1,
      paddingTop:50
    }
  
  }))

  export default useStyles