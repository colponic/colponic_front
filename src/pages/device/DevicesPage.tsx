import {
  useState,
  useEffect,
  useCallback,
  useMemo
} from "react";
import DeviceBox from "../../components/device_box/DeviceBox";
import {CircularProgress, Grid, Typography, Container} from "@material-ui/core";
import useStyles from "./styles";
import ProcessStatusContent from "../../components/ProcessStatusContent";
import {
  Exception,
  Idle,
  Processing,
  ProcessStatus,
  Success,
} from "../../core/Process";
import Device from "../../models/Device";
import { useAxiosAuthInstance } from "../../contexts/AxiosAuthContext";
import {DeviceService} from "../../services/DeviceService";
import DeviceImage from "../../assets/images/tablet-min.jpg"

const DevicesPage = () =>{ 
  const classes = useStyles();
  const axios = useAxiosAuthInstance();

  const deviceService = useMemo(() => {
    return new DeviceService(axios);
  }, [axios]);

  const [deviceProcess, setDeviceProcess] = useState<
    ProcessStatus<Device[]>
    >(new Idle());

  const loadDevices = useCallback(async () => {
    try {
      setDeviceProcess(new Processing());
      const devices: Device[] = await deviceService.getDevices();
      setDeviceProcess(new Success(devices));
    } catch (e:any) {
      setDeviceProcess(new Exception(e));
    }
  }, [deviceService]);

  useEffect(() => {
    loadDevices();
  }, [loadDevices]);

  return (
  <Grid container className={classes.root}>
    <Grid
      item
      xs={false}
      sm={4}
      md={6}
      style={{
        backgroundImage: `url(${DeviceImage})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
    />
    <Grid item xs={false} className={classes.containerDevices}>
      <ProcessStatusContent
        processStatus={deviceProcess}
        idleContent={null}
        processingContent={
          <>
            <CircularProgress />
          </>
        }
        exceptionContentFactory={null}
        successContentFactory={(value)=>(
          <Container maxWidth={"sm"}>
            <Typography variant={"h3"} gutterBottom={true}>Mis dispositvos</Typography>
            {value.map((device:Device) =>{
              return <DeviceBox key={device.id} device={device}/>
            })}
          </Container>
        )

        }
        sameExceptionAsIdle={true}
      />

    </Grid>
  </Grid>

    
  );
}
 
  
  
export default DevicesPage