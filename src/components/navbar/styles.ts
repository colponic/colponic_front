import { makeStyles, createStyles, Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: "#C0C0BF",
      height: 80,
      display: "flex",
      flexDirection: "row",
      padding: "0 10vw",
      alignItems: "center",
    },
    space: {
      flexGrow: 1,
    },
    iconStyle: {
      marginLeft: 25,
      cursor: "pointer",
      width: 36,
    },
  })
);
export default useStyles;
