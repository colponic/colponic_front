import useStyles from "./styles";
import ColponicLogo from "../../assets/logos/colponic.png";
import { ReactComponent as LogoOutIcon } from "../../assets/icons/icon_log_out.svg";
import { ReactComponent as HomeIcon } from "../../assets/icons/icon_home.svg";
import { ReactComponent as ListIcon } from "../../assets/icons/icon_list.svg";
import { ReactComponent as AlarmIcon } from "../../assets/icons/icon_alarm.svg";
import { useHistory, useLocation } from "react-router-dom";
import { useAuth } from "../../contexts/AuthContext";

interface Location {
  pathname: string;
  state?: object;
}

const Navbar = () => {
  const classes = useStyles();
  const history = useHistory();
  const auth = useAuth();
  const location: Location = useLocation();
  let deviceId = "";
  if (location.state && location.state.hasOwnProperty("deviceId")) {
    // @ts-ignore
    deviceId = location.state["deviceId"];
  }

  return (
    <div className={classes.root}>
      <a href={"/"}>
        <img src={ColponicLogo} alt={"Colponic"} height={45} />
      </a>
      <div className={classes.space} />
      {location.pathname === "/alarms" || location.pathname === "/dashboard" ? (
        <ListIcon
          className={classes.iconStyle}
          onClick={() => {
            history.push("/devices");
          }}
        />
      ) : null}
      {location.pathname === "/alarms" ? (
        <HomeIcon
          className={classes.iconStyle}
          onClick={() => {
            history.push({
              pathname: "/dashboard",
              state: { deviceId: deviceId },
            });
          }}
        />
      ) : null}
      {location.pathname === "/dashboard" ? (
        <AlarmIcon
          className={classes.iconStyle}
          onClick={() => {
            history.push({
              pathname: "/alarms",
              state: { deviceId: deviceId },
            });
          }}
        />
      ) : null}
      <LogoOutIcon
        className={classes.iconStyle}
        onClick={async () => {
          await auth.logOut();
          history.push("/login");
        }}
      />
    </div>
  );
};

export default Navbar;
