import { makeStyles, createStyles, Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      width: 400,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      height: "fit-content",
      paddingBottom: 20,
      border: "1px dashed #111111",
      borderRadius: 8,
    },
    titleContainer: {
      width: "100%",
      backgroundColor: "#C0C0BF",
      color: "black",
      height: 50,
      fontSize: "1.5rem",
      fontWeight: 600,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      // borderRadius: "10px 10px 0 0",
      marginBottom: 20,
      borderBottom: "1px dashed #111111",
    },
    image: {
      borderRadius: "50%",
      width: 200,
      border: "5px solid #FFFFFF",
      marginBottom: 20,
    },
    infoContainer: {
      display: "flex",
      flexDirection: "row",
      width: "90%",
      borderRadius: 8,
    },
    column: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-between",
    },
    info: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-around",
      alignItems: "center",
      "&:hover": {
        backgroundColor: "#F0F0F0",
      },
    },
    infoText: {
      fontSize: "1.5rem",
      textAlign: "center",
      flexGrow: 1,
    },
    infoDate: {
      fontSize: "0.9rem",
      textAlign: "center",
      flexGrow: 1,
    },
  })
);
export default useStyles;
