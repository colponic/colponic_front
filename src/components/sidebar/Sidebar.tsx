import useStyles from "./styles";
import {
  BadgeSharp,
  HourglassEmptySharp,
  HourglassFullSharp,
  RoomSharp,
  RouterSharp,
} from "@mui/icons-material";
import { useAxiosAuthInstance } from "../../contexts/AxiosAuthContext";
import { useCallback, useEffect, useMemo, useState } from "react";
import { DeviceService } from "../../services/DeviceService";
import {
  Exception,
  Idle,
  Processing,
  ProcessStatus,
  Success,
} from "../../core/Process";
import Device from "../../models/Device";
import ProcessStatusContent from "../ProcessStatusContent";
import { CircularProgress } from "@material-ui/core";
import { useLocation } from "react-router-dom";

const Sidebar = () => {
  const classes = useStyles();
  const axios = useAxiosAuthInstance();
  const location = useLocation();
  // @ts-ignore
  const deviceId = location.state["deviceId"];
  const deviceService = useMemo(() => {
    return new DeviceService(axios);
  }, [axios]);

  const [deviceProcess, setDeviceProcess] = useState<ProcessStatus<Device>>(
    new Idle()
  );

  const loadDevices = useCallback(async () => {
    try {
      setDeviceProcess(new Processing());
      const device: Device = await deviceService.getDevice(deviceId);
      setDeviceProcess(new Success(device));
    } catch (e: any) {
      setDeviceProcess(new Exception(e));
    }
  }, [deviceService, deviceId]);

  useEffect(() => {
    loadDevices();
  }, [loadDevices]);
  return (
    <ProcessStatusContent
      processStatus={deviceProcess}
      idleContent={null}
      processingContent={
        <>
          <CircularProgress />
        </>
      }
      exceptionContentFactory={null}
      successContentFactory={(value) => (
        <div className={classes.container}>
          <div className={classes.titleContainer}>
            <p>Información del Módulo</p>
          </div>
          <img
            src={value.photo ? value.photo : "https://picsum.photos/200"}
            alt={"profile"}
            className={classes.image}
          />
          <div className={classes.infoContainer}>
            <div className={classes.column}>
              <div
                style={{
                  margin: "0 10px",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <RouterSharp sx={{ fontSize: 48, paddingBottom: 0 }} />
                <p style={{ margin: 0 }}>Módulo</p>
              </div>
              <div
                style={{
                  margin: "0 10px",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <BadgeSharp sx={{ fontSize: 48, paddingBottom: 0 }} />
                <p style={{ margin: 0 }}>Id</p>
              </div>
              <div
                style={{
                  margin: "0 10px",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <RoomSharp sx={{ fontSize: 48, paddingBottom: 0 }} />
                <p style={{ margin: 0 }}>Ubicación</p>
              </div>
              <div
                style={{
                  margin: "0 10px",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <HourglassEmptySharp sx={{ fontSize: 48, margin: "0 10px" }} />
                <p style={{ margin: 0 }}>Cultivo</p>
              </div>
              <div
                style={{
                  margin: "0 10px",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <HourglassFullSharp sx={{ fontSize: 48, margin: "0 10px" }} />
                <p style={{ margin: 0 }}>Cosecha</p>
              </div>
            </div>
            <div className={classes.column}>
              <p className={classes.infoText}>{value.name}</p>
              <p className={classes.infoText}>{value.id}</p>
              <p className={classes.infoText}>{value.location}</p>
              <p className={classes.infoText}>
                {new Intl.DateTimeFormat("es-CO", {
                  year: "numeric",
                  month: "short",
                  day: "numeric",
                }).format(new Date(value.startDate))}
              </p>
              <p className={classes.infoText}>
                {new Intl.DateTimeFormat("es-CO", {
                  year: "numeric",
                  month: "short",
                  day: "numeric",
                }).format(new Date(value.endDate))}
              </p>
            </div>
          </div>
        </div>
      )}
      sameExceptionAsIdle={true}
    />
  );
};

export default Sidebar;
