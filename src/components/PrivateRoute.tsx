import {Route, Redirect} from "react-router-dom";
import {useAuth} from "../contexts/AuthContext";

const PrivateRoute = ({children, ...rest}: any) => {
  const auth = useAuth();
  return (
    <Route
      {...rest}
      render={({location}) => {
        if (auth.isLoggedIn) {
          return children
        } else {
          return (<Redirect
            to={{
              pathname: "/login",
              search: `${(location.pathname === '/') ? '' : `?next=${location.pathname}`}`
            }}
          />)
        }
      }
      }/>
  )
}

export default PrivateRoute;