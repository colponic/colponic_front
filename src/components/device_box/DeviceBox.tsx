import Device from "../../models/Device";
import useStyles from "./styles.sh";
import { Button } from "@material-ui/core";
import { ReactComponent as ConnectedIcon } from "../../assets/icons/icon_device_connected.svg";
import { ReactComponent as DisconnectedIcon } from "../../assets/icons/icon_device_disconnected.svg";
import { useHistory } from "react-router-dom";

interface Props {
  device: Device;
}

const DeviceBox = ({ device }: Props) => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div className={classes.root}>
      {device.state ? <ConnectedIcon /> : <DisconnectedIcon />}
      <div className={classes.containerText}>
        <p className={classes.nameDevice}>{device.name}</p>
        <p className={classes.connectDevice}>
          {device.state ? "Conectado" : "Desconectado"}
        </p>
      </div>
      <Button
        color={"primary"}
        variant={"contained"}
        className={classes.buttonAccess}
        onClick={() => {
          history.push({
            pathname: "/dashboard",
            state: { deviceId: device.id },
          });
        }}
        disabled={!device.state}
      >
        Acceder
      </Button>
    </div>
  );
};
export default DeviceBox;
