import {makeStyles, createStyles, Theme} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      cursor: "pointer",
      border: "1px solid #333333",
      padding: "20px 50px",
      display: "flex",
      flexDirection:"row",
      margin:1,
      position: "relative",
      height:100
    },
    containerText:{
      marginLeft: 20
    },
    nameDevice:{
      fontWeight:600,
      fontSize:"1.5rem",
      margin: 0,
      padding: 0
    },
    connectDevice:{
      fontWeight:200,
      fontSize:"1rem",
      margin:0,
      padding: 0
    },
    buttonAccess:{
      position:"absolute",
      bottom: 10,
      right:10
    }
  })
)
export default useStyles